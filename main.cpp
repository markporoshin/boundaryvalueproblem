#include <iostream>


void startFiniteDiff(int mode);
void start2CPMethod(int mode);
void start2CPMethodByRunge(int mode);

int main(int size, char ** args) {

    switch (std::stoi(std::string(args[1]))) {
        case 0:
            start2CPMethod(0);
            break;
        case 1:
            startFiniteDiff(1);
            break;
        case 2:
            start2CPMethodByRunge(2);
            break;
    }

    return 0;
}
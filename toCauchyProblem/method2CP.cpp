//
// Created by Марк on 21.05.19.
//

#include <cmath>
#include <vector>
#include <iostream>


using namespace std;

unsigned counter = 0;

static double px(double x) {
    counter++;
    return -1 / (1 + exp(x));
}
static double qx(double x) {
    counter++;
    return -exp(x) / (1 + exp(x));
}
static double fx(double x) {
    counter++;
    return exp(x) / (1 + exp(x));
}
//static double px(double x) {
//    return -2;
//}
//static double qx(double x) {
//    return -1;
//}
//static double fx(double x) {
//    counter++;
//    return cos(x);
//}
static double dy(double x, double y, double z) {
    return z;
}
static double dz1(double x, double y, double z) {
    return   fx(x) - px(x) * z - qx(x) * y;
}
static double dz0(double x, double y, double z) {
    return         - px(x) * z - qx(x) * y;
}






static double k1(double h, double x, double y, double z,
                  double (*d)(double x, double y, double z)) {
    return h * d(x, y, z);
}
static double k2(double h, double x, double y, double z,
                 double (*d1)(double x, double y, double z),
                 double (*d2)(double x, double y, double z)) {
    return h * d1(x + h/2, y + k1(h, x, y, z, d1) / 2, z + k1(h, x, y, z, d2) / 2);
}
static pair<vector<double>, vector<double>> solveSLAE2(unsigned  N, double h,
                                                      vector<double> & xh, double y0, double z0,
                                                      double (*dy)(double x, double y, double z),
                                                      double (*dz)(double x, double y, double z)) {
    vector<double> yh(N); yh[0] = y0;
    vector<double> zh(N); zh[0] = z0;

    for (int i = 0; i < N-1; ++i) {
        yh[i+1] = yh[i] + k2(h, xh[i], yh[i], zh[i], dy, dz);
        //yh[i+1] = yh[i] + h * dy(xh[i], yh[i], zh[i]);
        //zh[i+1] = zh[i] + h * dz(xh[i], yh[i], zh[i]);
        zh[i+1] = zh[i] + k2(h, xh[i], yh[i], zh[i], dz, dy);
    }
    return {yh, zh};
}

static pair<vector<double>, vector<double>> solveSLAE(unsigned  N, double h,
                                                      vector<double> & xh, double y0, double z0,
                                                      double (*dy)(double x, double y, double z),
                                                      double (*dz)(double x, double y, double z)) {
    vector<double> yh(N); yh[0] = y0;
    vector<double> zh(N); zh[0] = z0;

    for (int i = 0; i < N-1; ++i) {
        //yh[i+1] = yh[i] + 1. / 2 * k2(h, xh[i], yh[i], zh[i], dy, dz);
        yh[i+1] = yh[i] + h * dy(xh[i], yh[i], zh[i]);
        zh[i+1] = zh[i] + h * dz(xh[i], yh[i], zh[i]);
        //zh[i+1] = zh[i] + 1. / 2 * k2(h, xh[i], yh[i], zh[i], dz, dy);
    }
    return {yh, zh};
}


pair<vector<double>, vector<double>> toCP(unsigned N, double x0, double y0, double xn, double yn) {
    double h = (xn - x0) / (N - 1);
    vector<double> xh(N, 0);
    vector<double> yh(N, 0);
    for (int i = 0; i < N; ++i) {
        xh[i] = x0 + (xn - x0) * i / (N - 1);
    }
    pair<vector<double>, vector<double>> SLAEU = solveSLAE2(N, h, xh, 0, 1, dy, dz0);
    pair<vector<double>, vector<double>> SLAEV = solveSLAE2(N, h, xh, y0, 0, dy, dz1);

    vector<double> u = SLAEU.first;
    vector<double> v = SLAEV.first;

    double C = (yn-v[N-1]) / u[N-1];
    for (int j = 0; j < N; ++j) {
        yh[j] = C * u[j] + v[j];
    }

    return {xh, yh};
};


pair<pair<vector<double>, vector<double>>, unsigned > toCPRunge(double E, double x0, double y0, double xn, double yn) {
    unsigned N = 2;
    double h = (xn - x0) / (N - 1);
    vector<double> xh(N, 0);

    for (int i = 0; i < N; ++i) {
        xh[i] = x0 + (xn - x0) * i / (N - 1);
    }


    pair<vector<double>, vector<double>> SLAEU = solveSLAE2(N, h, xh, 0, 1, dy, dz0);
    pair<vector<double>, vector<double>> SLAEU_last;
    pair<vector<double>, vector<double>> SLAEV = solveSLAE2(N, h, xh, y0, 0, dy, dz1);
    pair<vector<double>, vector<double>> SLAEV_last;


    double a;
    double b;
    do {
        SLAEU_last = SLAEU;
        SLAEV_last = SLAEV;
        N *= 2;
        xh = vector<double>(N);
        h = (xn - x0) / (N - 1);
        for (int i = 0; i < N; ++i)
            xh[i] = x0 + (xn - x0) * i / (N - 1);

        SLAEU = solveSLAE2(N, h, xh, 0, 1, dy, dz0);
        SLAEV = solveSLAE2(N, h, xh, y0, 0, dy, dz1);


        a = fabs(SLAEU.first.back() - SLAEU_last.first.back());
        b = fabs(SLAEV.first.back() - SLAEV_last.first.back());
    } while (fabs(SLAEU.first.back() - SLAEU_last.first.back()) > E ||
             fabs(SLAEV.first.back() - SLAEV_last.first.back()) > E);

    vector<double> yh(N, 0);
    vector<double> u = SLAEU.first;
    vector<double> v = SLAEV.first;

    double C = (yn-v[N-1]) / u[N-1];
    for (int j = 0; j < N; ++j) {
        yh[j] = C * u[j] + v[j];
    }

    return {{xh, yh}, N};
};





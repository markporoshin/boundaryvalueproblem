//
// Created by Марк on 19.05.19.
//


#include <vector>
#include <iostream>

using namespace std;



static double Ai(double xi, double (*px)(double), double h) {
    return 1. - px(xi) * h / 2;
}

static double Bi(double xi, double (*px)(double), double h) {
    return 1. + px(xi) * h / 2;
}

static double Ci(double xi, double (*qx)(double), double h) {
    return 2. - qx(xi) * h * h;
}

static double Fi(double xi, double (*fx)(double), double h) {
    return fx(xi) * h * h;
}

static vector<double> solveSLAE(vector<vector<double>> & A, vector<double> & B, unsigned N) {
    vector<double> res(N);

    vector<double> betta(N);
    vector<double> lambda(N);

    vector<double> c(N, 0), d(N, 0), b(N, 0);
    c[0] = A[0][0]; d[0] = A[0][1];
    for (int j = 1; j < N-1; ++j) {
        c[j] = A[j][j];
        d[j] = A[j][j+1];
        b[j] = A[j][j-1];
    }
    c[N-1] = A[N-1][N-1];
    b[N-1] = A[N-1][N-2];


    betta[0] = -d[0] / c[0];
    lambda[0] = B[0] / c[0];
    for (int i = 1; i < N; ++i) {
        //double ri = (c[i] + b[i] * betta[i-1]);
        //double di = d[i];
        //double ni = (B[i] - b[i] * lambda[i-1]);
        //double Bi = B[i];

        betta[i] = - d[i] / (c[i] + b[i] * betta[i-1]);
        lambda[i] = (B[i] - b[i] * lambda[i-1]) / (c[i] + b[i] * betta[i-1]);

        //cout << i << " " << bi << " " << li << endl;
    }

    res[N-1] = lambda[N-1];
    for (int i = N-2; i >= 0 ; --i) {
        res[i] = betta[i] * res[i + 1] + lambda[i];
    }
    return res;
}

pair<vector<double>, vector<double>> solveDiffByFiniteDiff(unsigned N,
                                                   double (*fx)(double),
                                                   double (*px)(double),
                                                   double (*qx)(double),
                                                   double x0, double y0,
                                                   double xn, double yn) {

    double h = (xn - x0) / (N - 1);
    vector<double> xh(N, 0);
    for (int i = 0; i < N; ++i) {
        xh[i] = x0 + (xn - x0) * i / (N - 1);
    }
    vector<vector<double>> A(N, vector<double>(N, 0));
    vector<double>  B(N, 0);
    A[0][0] = 1; B[0] = y0;
    for (int i = 1; i < N-1; i++) {
        A[i][i-1] = Ai(xh[i], px, h);
        A[i][i] = -Ci(xh[i], qx, h);
        A[i][i+1] = Bi(xh[i], px, h);
        B[i] = Fi(xh[i], fx, h);
    }
    A[N-1][N-1] = 1; B[N-1] = yn;

    vector<double> yh = solveSLAE(A, B, N);

    return {xh, yh};
};
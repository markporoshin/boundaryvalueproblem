//
// Created by Марк on 19.05.19.
//



#include <vector>
#include <string>
#include <fstream>
#include <cmath>
#include <iostream>
#include <iomanip>

using namespace std;



pair<vector<double>, vector<double>> n2CauchyProblems(unsigned N,
                                                      double (*fx)(double),
                                                      double (*px)(double),
                                                      double (*qx)(double),
                                                      double x0, double y0,
                                                      double xn, double yn);


static double px(double x) {
    return -1 / (1 + exp(x));
}
static double qx(double x) {
    return -exp(x) / (1 + exp(x));
}
static double fx(double x) {
    return exp(x) / (1 + exp(x));
}
static vector<double> accurateDecision(vector<double> xh) {
    vector<double> res;
    for (double x : xh) {
        res.push_back(exp(x)-1);
    }
    return res;
}

static void readConfig(unsigned & N, double & x0, double & y0, double & xn, double & yn, string file) {
    ifstream in(file);
    in >> N >> x0 >> y0 >> xn >> yn;
}
static void writeTableFun(int N, pair<vector<double>, vector<double>> tf, string file) {
    ofstream out(file);
    out << N << endl;
    for (int i = 0; i < N; ++i) {
        out << std::setprecision(15) << tf.first[i] << " " << tf.second[i] << endl;
    }
}

void startn2Method(int mode) {
    unsigned N;
    double x0, y0;
    double xn, yn;
    readConfig(N, x0, y0, xn, yn, "/Users/mark/CLionProjects/BoundaryValueProblem/in.txt");
    pair<vector<double>, vector<double>> tf = n2CauchyProblems(N, fx, px, qx, x0, y0, xn, yn);
    writeTableFun(N, tf, "/Users/mark/CLionProjects/BoundaryValueProblem/out.txt");

    //vector<double> yh = accurateDecision(tf.first);
    //for (auto y : yh) cout << y << endl;
}
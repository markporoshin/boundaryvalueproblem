//
// Created by Марк on 19.05.19.
//



#include <vector>
#include <functional>

using namespace std;

typedef std::function<double(double,double,double)> fundddd;


static double rungeStep(double y, double x0, double x1,
                        double h,
                        fundddd &f,
                        fundddd & fx1) {
#define alpha 1.
    return y + h * ((1.-alpha) * f(x0, x1, y) + alpha * f(x0 + h / (2 * alpha),
                                                          x1 + fx1(x0, y, x1) / (2 * alpha),
                                                          y + f(x0, x1, y) / (2 * alpha)));
}

static pair<vector<double>, vector<double>> solveSystem2ByRugne2(int N, double h, double y0, double z0,
                                                          vector<double> & xh,
                                                                 fundddd & f1,
                                                                 fundddd & f2) {
    vector<double> yh; yh.push_back(y0);
    vector<double> zh; zh.push_back(z0);

    for (int i = 0; i < N-1; ++i) {
        yh.push_back(rungeStep(yh[i], xh[i], zh[i], h, f2, f1));
        zh.push_back(rungeStep(zh[i], xh[i], yh[i], h, f1, f2));
    }
    return {yh, zh};
};


static vector<double> solveLinerDiff2Level(unsigned N,
                                           double (*fx)(double),
                                           double (*px)(double),
                                           double (*qx)(double),
                                           double x0, double xn, double y0, double dy0) {
    double h = (xn - x0) / (N - 1);
    vector<double> xh(N, 0);
    for (int i = 0; i < N; ++i) {
        xh[i] = x0 + (xn - x0) * i / (N - 1);
    }
    fundddd f1 = [fx, px, qx](double x, double y, double z) -> double { return (fx(x) - px(x) * z - qx(x) * y); };
    fundddd f2 =           [](double x, double z, double y) -> double { return z; };

    return solveSystem2ByRugne2(N, h, y0, dy0, xh, f1, f2).first;
}





pair<vector<double>, vector<double>> n2CauchyProblems(unsigned N,
                                                      double (*fx)(double),
                                                      double (*px)(double),
                                                      double (*qx)(double),
                                                      double x0, double y0,
                                                      double xn, double yn) {
    double h = (xn - x0) / (N - 1);
    vector<double> xh(N, 0);
    vector<double> yh(N, 0);
    for (int i = 0; i < N; ++i) {
        xh[i] = x0 + (xn - x0) * i / (N - 1);
    }

    {
        double u0 = 0, du0 = -1;
        double v0 = y0, dv0 = 0;

        vector<double> u = solveLinerDiff2Level(N, fx, px, qx, x0, xn, u0, du0);
        vector<double> v = solveLinerDiff2Level(N, fx, px, qx, x0, xn, v0, dv0);

        double C = (yn - v[N-1]) / u[N-1];
        for (int i = 0; i < N; ++i) {
            yh[i] = C * u[i] + v[i];
        }
    }

    return {xh, yh};
}